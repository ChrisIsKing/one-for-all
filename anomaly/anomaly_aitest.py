"""
Module for testing anomaly detection functionality.
"""

import unittest
from numpy import isclose

from utils.clinc.environment import get_package_path
from aiutils.anomalydetection.embed_utils import load_data_list
from aiutils.anomalydetection.AnomalyDetector import (
    AnomalyDetectorUSE, AnomalyDetectorSIF
)

FILE_SMALL = get_package_path(
    'aiutils/anomalydetection/testdata/test_small.json'
)


class AnomalyDetectorTest(unittest.TestCase):
    """
    Test Anomaly Detector functionality.
    """

    @classmethod
    def setUpClass(cls):
        """
        Pre-test setup.
        """
        cls.ad_sif = AnomalyDetectorSIF('euclidean')
        cls.ad_use = AnomalyDetectorUSE('euclidean')
        cls.data = load_data_list(FILE_SMALL)

    def test_sif_small_data(self):
        """
        Test functionality of SIF model on test_small.json.
        """
        self.ad_sif.set_distance_measure('euclidean')
        self.ad_sif.embed_data(self.data)
        ranked_list = self.ad_sif.sort_data()
        pred_top = ranked_list[0]
        pred_bot = ranked_list[-1]
        expected_top = ('can i place an order', 0.11813426379414015)
        expected_bot = ('what is my balance', 0.019591431930518398)
        self.assertEqual(expected_top[0], pred_top[0])
        self.assertEqual(expected_bot[0], expected_bot[0])
        self.assertTrue(isclose(expected_top[1], pred_top[1]))
        self.assertTrue(isclose(expected_bot[1], pred_bot[1]))

    def test_use_small_data(self):
        """
        Test functionality of USE model on test_small.json.
        """
        self.ad_use.set_distance_measure('euclidean')
        self.ad_use.embed_data(self.data)
        ranked_list = self.ad_use.sort_data()
        pred_top = ranked_list[0]
        pred_bot = ranked_list[-1]
        expected_top = ('i\'d like a sprite', 0.9828383176734428)
        expected_bot = ('can you give me my balance', 0.5010728948833759)
        self.assertEqual(expected_top[0], pred_top[0])
        self.assertEqual(expected_bot[0], expected_bot[0])
        self.assertTrue(isclose(expected_top[1], pred_top[1]))
        self.assertTrue(isclose(expected_bot[1], pred_bot[1]))

if __name__ == '__main__':
    unittest.main()
