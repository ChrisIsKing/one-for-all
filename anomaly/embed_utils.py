"""
Utility Functions for Clustering and Anamoly Detection
"""

import codecs
import json
import os
import numpy as np
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from sklearn.decomposition import TruncatedSVD
from sklearn.cluster import KMeans
import tensorflow as tf
import tensorflow_hub as hub

from anomaly.text_util import simple_tokenize

STOPWORDS = stopwords.words('english')
GLOVE_MODEL_DIR = 'utils'
GLOVE_MODEL_PATH = os.path.join(GLOVE_MODEL_DIR, 'glove.6B.50d.txt')
USE_MODEL_DIR = 'utils/use/Universal_Sentence_Encoder'


def load_glove_model(filename=GLOVE_MODEL_PATH):
    """
    Load word embeddings (Recommended to use GLove)
    filename: string of file location
    returns dictionary with words as keys, and embeddings as values
    """
    print("Loading GloVe Word Embeddings")
    model = {}
    filer = codecs.open(filename, encoding='utf-8')
    for line in filer:
        split_line = line.split()
        word = split_line[0]
        embedding = np.array([float(val) for val in split_line[1:]])
        model[word] = embedding
    print("Loaded GloVe Word Embeddings")
    return model

def load_data_list(filename):
    """
    Load dataset of sentences from a JSON list or JSON dict
    filename: string for file location
    returns a list of sentences
    """
    filer = codecs.open(filename, encoding='utf-8')
    data = json.load(filer)
    if isinstance(data, list):
        return data
    if isinstance(data, dict):
        return list(data.keys())
    return data


def euclidean_distance(vec_a, vec_b):
    """
    Euclidean Distance between vectors a and b
    vec_a: numpy vector
    vec_b: numpy vector
    returns euclidean L2 distance between the two vectors
    """
    return np.linalg.norm(vec_a - vec_b)


def cosine_similarity(vec_a, vec_b, scale=False):
    """
    Cosine Similarity between vectors a and b
    vec_a: numpy vector
    vec_b: numpy vector
    scale: boolean; if False, then don't scale by 1/norm(vec_b)
    """
    l2norm = np.linalg.norm
    score = np.dot(vec_a, vec_b)
    if scale is True:
        score = score / float(l2norm(vec_a) * l2norm(vec_b))
    else:
        score = score / float(l2norm(vec_a))
    return float(score)


def cosine_distance(vec_a, vec_b):
    """
    1 - Cosine Similarity
    """
    return 1 - cosine_similarity(vec_a, vec_b, scale=True)


def word_distance(first_word, second_word, model):
    """
    Euclidean Distance between two words
    first_word: string for word
    second_word: string for word
    model: word embedding dictionary
    """
    return euclidean_distance(model[first_word], model[second_word])


def show_closest(model, word, number):
    """
    Shows words closest to a given word
    model: word embedding dictionary
    word: word under consideration
    number: number of closest words to return
    returns a list of words with length 'number'
    """
    distance_dict = {}
    word_list = []
    for key in model:
        distance_dict[key] = word_distance(key, word, model)
    for _ in range(number):
        word = min(distance_dict.items(), key=lambda x: x[1])
        word_list.append(word)
        del distance_dict[word[0]]
    return word_list


def embed_sentence_stmean(sentence, model, filter_stop):
    """
    Embed sentence using standard mean of word vectors
    sentence: string for sentence to be embeded
    model: dictionary for glove vectors
    filter_stop: boolean for whether or not to filter stop words
    returns embedding for sentence as numpy vector
    """
    if filter_stop is True:
        sentence = filter_sentence(sentence)
    if filter_stop is False:
        sentence = word_tokenize(sentence)
    embedding_mean = np.array(0)
    length = len(sentence)
    for i in sentence:
        if i.lower() in model:
            embedding_mean = np.add(model[i.lower()], embedding_mean)
        else:
            length -= 1

    if length != 0:
        embedding_mean = embedding_mean / length
    return embedding_mean


def embed_sentences_mean(sentences, model, filter_stop):
    """
    Embed sentences using un-weighted mean of word embeddings.
    """
    sentence_representations = []
    for sentence in sentences:
        embedding = embed_sentence_stmean(sentence,
                                          model,
                                          filter_stop)
        sentence_representations.append(embedding)
    return sentence_representations


def filter_sentence(sentence):
    """
    Filter out stop words from sentence
    sentence: string containing sentence
    returns a tokenized list of words with stop words removed
    """
    tokenized_sentence = word_tokenize(sentence)
    filtered_sentence = []
    for word in tokenized_sentence:
        if word not in STOPWORDS:
            filtered_sentence.append(word)
    return filtered_sentence


def estimated_word_frequency(word, list_data):
    """
    Returns frequency of a word given total number of words
    word: word under consideration
    list_data: list of sentences
    """
    total = total_num_words(list_data)
    word_count = 0
    for i in list_data:
        word_count = word_count + i.count(word)
    return word_count / total


def total_num_words(data):
    """
    Finds total number of words in dataset
    data: list of sentences
    """
    count = 0
    for sentence in data:
        count = count + len(simple_tokenize(sentence))
    return count


def word_weightage(word, data, param):
    """
    Runs SIF weightage
    word: word under consideration
    data: list of sentences
    returns smooth inverse frequency of word
    """
    return param / (param + estimated_word_frequency(word, data))


def sif_embed_sent(sentence, model, param, data, filter_stop):
    """
    Returns SIF weighted average sentence
    sentence: sentence as string
    model: dictionary of embeddings
    param: parameter for SIF mean
    data: list of sentences
    filter_stop: boolean for whether or not to filter stopwords
    """
    if filter_stop is True:
        sentence = filter_sentence(sentence)
    else:
        sentence = word_tokenize(sentence)
    embedding_mean = np.array(0)
    length = len(sentence)
    for token in sentence:
        token_lower = token.lower()
        if token_lower in model:
            weighted = word_weightage(token_lower, data, param) \
                       *np.array(model[token_lower])
            embedding_mean = np.add(weighted, embedding_mean)
        else:
            length -= 1
    if length != 0:
        embedding_mean = embedding_mean / length
    return embedding_mean


def universal_embedding(sentences):
    """
    Uses Universal Sentence Encoder
    in order to embed list of sentences
    sentences: list of sentences
    returns a list of embeddings for 'sentences'
    """
    with tf.Graph().as_default():
        embed = hub.Module(USE_MODEL_DIR)
        embeddings = embed(sentences)
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            sess.run(tf.tables_initializer())
            output = sess.run(embeddings)
    return output


def remove_pc_projection(data_list, n_c):
    """
    Subtracts off princple component projections
    data_list = list of sentence embeddings
    n_c = number of components/iterations to remove PC projections
    returns list of embeddings after removing principal component projections
    """
    svd = TruncatedSVD(n_components=n_c, n_iter=7, random_state=0)
    svd.fit(data_list)
    comp = svd.components_
    data_vec = np.array(data_list)
    if n_c == 1:
        reduced = data_vec - data_vec.dot(comp.transpose()) * comp
    else:
        reduced = data_vec - (data_vec.dot(comp.transpose()).dot(comp))
    return reduced


def cluster_data(num_clusters, data):
    """
    Runs kmeans clustering on data
    num_clusters: integer for number of clusters
    data: list of sentences
    returns an sklearn KMeans object fit on data
    """
    kmeans = KMeans(n_clusters=num_clusters)
    kmeans.fit(data)
    return kmeans
