"""
class definitions for AnomalyDetector and children
"""

import numpy as np

from anomaly.embed_utils import (
    sif_embed_sent, universal_embedding, remove_pc_projection,
    cluster_data, load_glove_model, euclidean_distance,
    cosine_distance, embed_sentences_mean
)

EUCLIDEAN = 'euclidean'
COSINE = 'cosine'
GLOVE = 'glove'


class AnomalyDetector:
    """
    Base class.
    """

    def __init__(self, distance_measure):
        self.data_dict = {}
        self.data = []
        self.ordered = []
        self.sentence_representations = []
        self.immediate_outliers = []
        self.set_distance_measure(distance_measure)
        self.name = ''

    def _clear_data(self):
        """
        Clear data lists and dictionaries.
        """
        self.data_dict = {}
        self.data = []
        self.ordered = []
        self.sentence_representations = []
        self.immediate_outliers = []

    def set_distance_measure(self, distance_measure):
        """
        Set the distance measure function.
        """
        self.distance_measure = None
        if distance_measure == EUCLIDEAN:
            self.distance_measure = euclidean_distance
        elif distance_measure == COSINE:
            self.distance_measure = cosine_distance

    def sort_data(self, k_clusters=1):
        """
        Sort data list in order of 'interesting-ness'. Consider the return
        value to be a ranked list. Sorting is based on distance from cluster
        center.
        similarity - a function for computing similarity score between two
            vectors.
        """
        similarity = self.distance_measure
        kmeans_data = np.array(self.sentence_representations, object)
        kmeans_cluster = cluster_data(k_clusters, kmeans_data)
        centroid = kmeans_cluster.cluster_centers_
        if self.distance_measure == COSINE:
            centroid = centroid.reshape(centroid.shape[0], -1).T
        for sentence, embedding in self.data_dict.items():
            self.ordered.append((sentence, similarity(embedding, centroid)))
        self.ordered.sort(key=lambda pair: pair[1], reverse=True)
        self.ordered += self.immediate_outliers
        return self.ordered

    def embed_data(self, data):
        """
        Embed sentences in data into embedding space.
        """

    def embed_and_sort(self, data):
        """
        Embed sentences in data to embedding space, then sort in order of
        'interesting-ness'.
        """

    def __call__(self, data):
        """
        Function call operator.
        """


class AnomalyDetectorUSE(AnomalyDetector):
    """
    Anomaly detector that uses Universal Sentence Encoder for sentence
    representations.
    """

    def __init__(self, distance_measure):
        AnomalyDetector.__init__(self, distance_measure)
        self.name = 'Anomaly Detector USE'

    def embed_data(self, data):
        """
        Embed sentences using USE.
        """
        self._clear_data()
        self.data = data
        self.sentence_representations = universal_embedding(self.data)
        for index, item in enumerate(self.data):
            self.data_dict[item] = self.sentence_representations[index]

    def embed_and_sort(self, data):
        """
        Embed sentences in data to USE embedding space, then sort in order of
        'interesting-ness'.
        """
        self.embed_data(data)
        return self.sort_data()

    def __call__(self, data):
        """
        Function call operator.
        """
        return self.embed_and_sort(data)


class AnomalyDetectorSIF(AnomalyDetector):
    """
    Anomaly detector that uses Smooth Inverse Frequency model (SIF) for sentence
    representations.
    """

    def __init__(self, distance_measure, embeddings_model=GLOVE):
        AnomalyDetector.__init__(self, distance_measure)
        self.name = 'Anomaly Detector SIF'
        self.embeddings_model_type = embeddings_model
        self.embeddings_model = None
        if self.embeddings_model_type == GLOVE:
            self.embeddings_model = load_glove_model()

    def embed_data(self,
                   data,
                   filter_stop=True,
                   a_scale_parameter=0.001,
                   truncated_svd_n_comp=2):
        """
        Embed sentences using SIF.
        """
        self._clear_data()
        self.data = data
        data_list = []
        for sentence in self.data:
            representation = sif_embed_sent(sentence.lower(),
                                            self.embeddings_model,
                                            a_scale_parameter,
                                            self.data,
                                            filter_stop)
            if representation.size != 1:
                data_list.append(representation)
            else:
                self.immediate_outliers.append(sentence)
        data_list = remove_pc_projection(data_list,
                                         truncated_svd_n_comp)
        for i, sentence in enumerate(self.data):
            self.data_dict[sentence] = data_list[i]
        self.sentence_representations = data_list

    def embed_and_sort(self,
                       data,
                       filter_stop=True,
                       a_scale_parameter=0.001,
                       truncated_svd_n_comp=2):
        """
        Embed sentences in data to SIF embedding space, then sort in order of
        'interesting-ness'.
        """
        self.embed_data(data,
                        filter_stop,
                        a_scale_parameter,
                        truncated_svd_n_comp)
        return self.sort_data()

    def __call__(self,
                 data,
                 filter_stop=True,
                 a_scale_parameter=0.001,
                 truncated_svd_n_comp=2):
        """
        Function call operator.
        """
        return self.embed_and_sort(data,
                                   filter_stop,
                                   a_scale_parameter,
                                   truncated_svd_n_comp)


class AnomalyDetectorAVE(AnomalyDetector):
    """
    Anomaly detector that uses average word embedding for sentence
    representations.
    """

    def __init__(self,
                 distance_measure,
                 embeddings_model=GLOVE):
        AnomalyDetector.__init__(self, distance_measure)
        self.name = 'Anomaly Detector AVE'
        self.embeddings_model_type = embeddings_model
        if self.embeddings_model_type == GLOVE:
            self.embeddings_model = load_glove_model()

    def embed_data(self, data, filter_stop=False):
        """
        Embed sentences using average word embedding.
        """
        self._clear_data()
        self.data = data
        self.sentence_representations = \
            embed_sentences_mean(self.data,
                                 self.embeddings_model,
                                 filter_stop)
        for index, item in enumerate(self.data):
            self.data_dict[item] = self.sentence_representations[index]

    def embed_and_sort(self, data):
        """
        Embed sentences in data to embedding space, then sort in order of
        'interesting-ness'.
        """
        self.embed_data(data)
        return self.sort_data()

    def __call__(self, data):
        """
        Function call operator.
        """
        return self.embed_and_sort(data)
