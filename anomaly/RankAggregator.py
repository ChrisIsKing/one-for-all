"""
Class definition for RankAggregator.
"""


class RankAggregator:
    """
    Base class.
    """

    def do_aggregation(self, ballots_list):
        """
        Perform rank aggregation on list of ranked lists.
        """


class BordaCount(RankAggregator):
    """
    Borda count rank aggregation.
    """

    def do_aggregation(self, ballots_list):
        """
        Perform Borda count rank aggregation. Function assumes lengths of
        lists in ballots_list are all equal.

        borda_candidates - ranked list of candidates
        borda_scores - corresponding borda count scores for candidates
        """
        score_map = {}
        ballot_length = len(ballots_list[0])
        for ballot in ballots_list:
            for i, candidate in enumerate(ballot):
                if candidate not in score_map:
                    score_map[candidate] = 0
                score_map[candidate] += ballot_length - i - 1
        score_pairs = list(score_map.items())
        score_pairs = sorted(score_pairs,
                             key=lambda pair: pair[1],
                             reverse=True)
        borda_candidates = [candidate for (candidate, _) in score_pairs]
        borda_scores = [borda_score for (_, borda_score) in score_pairs]
        return borda_candidates, borda_scores


class AverageRank(RankAggregator):
    """
    Average rank rank aggregation.
    """

    def do_aggregation(self, ballots_list):
        """
        Perform average rank rank aggregation. Function assumes lenghts of
        lists in ballots_list are all equal.

        ar_candidates - ranked list of candidates
        ar_scores - corresponding average rank scores for candidates
        """
        ranks_map = {}
        for ballot in ballots_list:
            for i, candidate in enumerate(ballot):
                if candidate not in ranks_map:
                    ranks_map[candidate] = []
                ranks_map[candidate].append(i + 1)
        for candidate in ranks_map.keys():
            ranks_list = ranks_map[candidate]
            ranks_map[candidate] = sum(ranks_list) / float(len(ranks_list))
        score_pairs = list(ranks_map.items())
        score_pairs = sorted(score_pairs,
                             key=lambda pair: pair[1])
        ar_candidates = [candidate for (candidate, _) in score_pairs]
        ar_scores = [score for (_, score) in score_pairs]
        return ar_candidates, ar_scores
