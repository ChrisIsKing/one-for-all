"""
Module for testing rank aggregation functionality.
"""

import unittest
from numpy import isclose

from aiutils.anomalydetection.RankAggregator import (
    BordaCount, AverageRank
)
from aiutils.anomalydetection.comparison import (
    kendall_tau, spearman_footrule, spearman_footrule_weighted
)

def _list_isclose(list_a, list_b):
    """
    Helper function to compare numeric values in two lists.
    """
    for i, item in enumerate(list_a):
        if not isclose(item, list_b[i]):
            return False
    return True


class RankAggregatorTest(unittest.TestCase):
    """
    Test rank aggregation functionality.
    """

    @classmethod
    def setUpClass(cls):
        """
        Pre-test setup.
        """
        cls.borda = BordaCount()
        cls.average_rank = AverageRank()

    def test_borda_count_same(self):
        """
        Test borda count functionality where ballots are the same.
        """
        ballot_1 = ['a', 'b', 'c']
        ballot_2 = ['a', 'b', 'c']
        ballots_list = [ballot_1, ballot_2]
        candidates, scores = self.borda.do_aggregation(ballots_list)
        expected_candidates = ['a', 'b', 'c']
        expected_scores = [4, 2, 0]
        self.assertEqual(candidates, expected_candidates)
        self.assertEqual(scores, expected_scores)

    def test_borda_count(self):
        """
        Test borda count functionality where ballots are different.
        """
        ballot_1 = ['a', 'b', 'c', 'd', 'e']
        ballot_2 = ['a', 'c', 'b', 'd', 'e']
        ballot_3 = ['c', 'a', 'b', 'd', 'e']
        ballots_list = [ballot_1, ballot_2, ballot_3]
        candidates, scores = self.borda.do_aggregation(ballots_list)
        expected_candidates = ['a', 'c', 'b', 'd', 'e']
        expected_scores = [11, 9, 7, 3, 0]
        self.assertEqual(candidates, expected_candidates)
        self.assertEqual(scores, expected_scores)

    def test_average_rank_same(self):
        """
        Test average rank functionality where ballots are the same.
        """
        ballot_1 = ['a', 'b', 'c']
        ballot_2 = ['a', 'b', 'c']
        ballots_list = [ballot_1, ballot_2]
        candidates, scores = self.average_rank.do_aggregation(ballots_list)
        expected_candidates = ['a', 'b', 'c']
        expected_scores = [1, 2, 3]
        self.assertEqual(candidates, expected_candidates)
        self.assertEqual(scores, expected_scores)

    def test_average_rank(self):
        """
        Test average rank functionality where ballots are different.
        """
        ballot_1 = ['a', 'b', 'c', 'd', 'e']
        ballot_2 = ['a', 'c', 'b', 'd', 'e']
        ballot_3 = ['c', 'a', 'b', 'd', 'e']
        ballots_list = [ballot_1, ballot_2, ballot_3]
        candidates, scores = self.average_rank.do_aggregation(ballots_list)
        expected_candidates = ['a', 'c', 'b', 'd', 'e']
        expected_scores = [4/float(3), 2.0, 8/float(3), 4.0, 5.0]
        self.assertEqual(candidates, expected_candidates)
        self.assertTrue(_list_isclose(expected_scores, scores))


class ComparisonTest(unittest.TestCase):
    """
    Test comparison functionality.
    """

    @classmethod
    def setUpClass(cls):
        """
        Pre-test setup.
        """
        cls.list_1 = ['a', 'd', 'c', 'b']
        cls.list_2 = ['a', 'c', 'd', 'b']
        cls.list_3 = ['b', 'd', 'c', 'a']

    def test_kendall_tau(self):
        """
        Test the kendall tau function.
        """
        tau_12_observed = kendall_tau(self.list_1, self.list_2)
        tau_13_observed = kendall_tau(self.list_1, self.list_3)
        tau_23_observed = kendall_tau(self.list_2, self.list_3)
        tau_12_expected = 4 / float(6)
        tau_13_expected = -4 / float(6)
        tau_23_expected = -1
        self.assertTrue(isclose(tau_12_observed, tau_12_expected))
        self.assertTrue(isclose(tau_23_observed, tau_23_expected))
        self.assertTrue(isclose(tau_13_observed, tau_13_expected))

    def test_spearman_footrule(self):
        """
        Test the spearman footrule function.
        """
        spf_12_observed = spearman_footrule(self.list_1, self.list_2)
        spf_13_observed = spearman_footrule(self.list_1, self.list_3)
        spf_23_observed = spearman_footrule(self.list_2, self.list_3)
        spf_12_expected = 1 + 1
        spf_13_expected = 3 + 3
        spf_23_expected = 3 + 1 + 1 + 3
        self.assertEqual(spf_12_observed, spf_12_expected)
        self.assertEqual(spf_13_observed, spf_13_expected)
        self.assertEqual(spf_23_observed, spf_23_expected)

    def test_spearman_footrule_weighted(self):
        """
        Test the weighted spearman footrule function.
        """
        wspf_12_observed = spearman_footrule_weighted(self.list_1, self.list_2)
        wspf_13_observed = spearman_footrule_weighted(self.list_1, self.list_3)
        wspf_23_observed = spearman_footrule_weighted(self.list_2, self.list_3)
        wspf_12_expected = 0.5 + 0.5
        wspf_13_expected = 3 + 3
        wspf_23_expected = 3 + 0.5 + 0.5 + 3
        self.assertTrue(isclose(wspf_12_observed, wspf_12_expected))
        self.assertTrue(isclose(wspf_13_observed, wspf_13_expected))
        self.assertTrue(isclose(wspf_23_observed, wspf_23_expected))

if __name__ == '__main__':
    unittest.main()
