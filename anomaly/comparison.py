"""
Functions for comparison of ranked lists.
"""

from itertools import combinations


def kendall_tau(list_a, list_b):
    """
    Kendall tau rank correlation between two full lists (a and b) of
    equal length. The pair (i,j) is concordant if item i is ranked
    higher or lower than item j in both ranked lists a and b.
    """
    combs = list(combinations(list_a, 2))
    n_con = 0 # n concordant pairs
    n_dis = 0 # n discordant pairs
    n_pair_choices = float(len(combs))
    for pair in combs:
        position_i_a = list_a.index(pair[0])
        position_j_a = list_a.index(pair[1])
        position_i_b = list_b.index(pair[0])
        position_j_b = list_b.index(pair[1])
        diff_a = position_i_a - position_j_a
        diff_b = position_i_b - position_j_b
        if diff_a * diff_b > 0:
            n_con += 1
        else:
            n_dis += 1
    score = (n_con - n_dis) / n_pair_choices
    return score


def spearman_footrule(list_a, list_b):
    """
    Spearman's footrule between two full ranked lists (a and b) of
    equal length.
    """
    rho = 0
    for i, item in enumerate(list_a):
        rho += abs(i - list_b.index(item))
    return rho


def spearman_footrule_weighted(list_a, list_b):
    """
    Spearman's weighted footrule between two full ranked lists (a and b)
    of equal length.
    """
    rho = 0
    for i, item in enumerate(list_a):
        position_i_b = list_b.index(item)
        abs_diff = abs(i - position_i_b)
        min_rank = min(i, position_i_b) + 1
        rho += abs_diff / float(min_rank)
    return rho


def pair_statstics(list_a, list_b):
    """
    Generate statistics for a pair of lists a and b. Meant for display
    purposes.
    """
    stats_dict = {}
    stats_dict['kendall\'s tau'] = kendall_tau(list_a, list_b)
    stats_dict['spearman\'s footrule'] = spearman_footrule(list_a, list_b)
    stats_dict['spearman\'s weighted footrule'] = \
        spearman_footrule_weighted(list_a, list_b)
    return stats_dict
