#!/usr/bin/env python
"""
Natural language text processing utility functions
"""

import string
import re
from threading import Lock
import inflect
import nltk
from nltk.sentiment.vader import SentimentIntensityAnalyzer
# from utils.clinc.contraction_expander import ContractionExpander
# from utils.clinc.top_level_domain import TopLevelDomain
# from utils.clinc.british_word_map import BRITISH_WORD_MAP
# from utils.clinc.foreign_currency import (
#     FOREIGN_CURRENCY_SYMBOLS as UNICODE_CURRENCY
# )
# from commons.SentimentAnalyzer import (
#     SENTIMENT_POSITIVE_THRESHOLD, SENTIMENT_NEGATIVE_THRESHOLD
# )

INFLECT_ENGINE = inflect.engine()

STOK_LOCK = Lock()
WTOK_LOCK = Lock()
STEM_LOCK = Lock()
LEMM_LOCK = Lock()
TAGG_LOCK = Lock()

# stemmer
_STEMMER = nltk.stem.snowball.SnowballStemmer('english')

# wordnet lemmatizer to remove plural
_WORDNET_LEMMA = nltk.stem.WordNetLemmatizer()

# contraction expander
# _CONTRACTION_EXPANDER = ContractionExpander()

# domain name identifier
# _TLD_IDENTIFIER = TopLevelDomain()

# Part-of-Speech tagger
_POS_TAGGER = nltk.tag.perceptron.PerceptronTagger()

# List of non-wordnet words that are plural
_OOV_PLURALS = ['txns']

# Sentiment analyzer
_SENTIMENT_ = SentimentIntensityAnalyzer()

def language_join(textlist, processor=None):
    """
    Join a list of text items into an natural language
    list. For example, if textlist=['A', 'B', 'C'], this
    method will return 'A, B and C'.

    Additionally, a processing function can be given to
    transform each list item. For example, a processor that
    appends the letter 's' to the input string in the above
    example would yield 'As, Bs and Cs'
    """
    def _identity(text):
        """
        identity function (the default processor function)
        """
        return text

    if processor is None:
        processor = _identity
    processed = [processor(text) for text in textlist]

    if len(processed) == 1:
        return processed[0]

    result = ', '.join(processed[0:-1])
    result += ' and ' + processed[-1]
    return result


def sentence_split(text):
    """
    Split text into a list of sentences
    """
    STOK_LOCK.acquire()
    result = nltk.sent_tokenize(text)
    STOK_LOCK.release()
    return result


def foreign_currency_split(token):
    """
    Split a token into separate tokens if unicode currency symbol is at start
    or end of token.
    """
    new_tokens = []
    for csym in UNICODE_CURRENCY:
        if token == csym:
            return [token]
        if token.startswith(csym):
            new_tokens.append(csym)
            token = token.replace(csym, '', 1)
            new_tokens.append(token)
            return new_tokens
        if token.endswith(csym):
            new_tokens.append(token[0:len(token) - len(csym)])
            new_tokens.append(csym)
            return new_tokens
    return [token]


def foreign_currency_tokenize(tokens):
    """
    If any token has unicode currency, then split that token.
    """
    new_tokens = []
    for token in tokens:
        new_tokens += foreign_currency_split(token)
    return new_tokens


def word_tokenize(text):
    """
    Split text into a list of word tokens.
    """
    WTOK_LOCK.acquire()
    result = nltk.word_tokenize(text)
    result = foreign_currency_tokenize(result)
    WTOK_LOCK.release()

    return result


def simple_tokenize(sentence):
    """
    Tokenize based on whitespace.
    """
    return sentence.split()


def sentiment_analysis(text):
    """
    Return a sentiment score of the given text, converted to -1(negative),
    0(neutral) or 1(positive) based on the cut-off defined in
    commons.SentimentAnalyzer
    """
    score = _SENTIMENT_.polarity_scores(text)['compound']
    if score <= SENTIMENT_NEGATIVE_THRESHOLD:
        return -1
    if score > SENTIMENT_POSITIVE_THRESHOLD:
        return 1
    return 0


def detokenize(tokenized_sentence):
    """
    tokenized_sentence: a list of strings (tokens)
    returns a string representing a sentence formed from tokens.

    example input:
    ["I", "'m", "did", "n't", "could", "'ve", "."]
    returns:
    "I'm didn't could've."

    code adapted from:
    stackoverflow.com/questions/21948019/python-untokenize-a-sentence
    """
    text = ' '.join(tokenized_sentence)
    step1 = text.replace("`` ", '"').replace(" ''", '"').replace('. . .', '...')
    step2 = step1.replace(" ( ", " (").replace(" ) ", ") ")
    step3 = re.sub(r' ([.,:;?!%]+)([ \'"`])', r"\1\2", step2)
    step4 = re.sub(r' ([.,:;?!%]+)$', r"\1", step3)
    step5 = step4.replace(" '", "'").replace(" n't", "n't").replace(
        "can not", "cannot")
    step6 = step5.replace(" ` ", " '")
    step7 = re.sub(r'[$]\s', r'$', step6)
    return step7.strip()


def expand_contraction(text):
    """
    Expands all contractions in text
    """
    return _CONTRACTION_EXPANDER.expand(text)


def is_domain(text):
    """
    Return True if text is a domain name
    """
    return _TLD_IDENTIFIER.is_domain(text)


def americanize_words(text):
    """
    Convert certain tokens to americanized spelling, if applicable.

    text - a string representing a query
    the text string is tokenized on whitespace.

    map:
    - 'cheque' --> 'check'
    """
    tokens = word_tokenize(text)
    for i, token in enumerate(tokens):
        tokens[i] = BRITISH_WORD_MAP.get(token, token)
    return detokenize(tokens)


def remove_punctuation(text):
    """
    Remove punctuation in the text
    """
    return ''.join((x for x in text if x not in string.punctuation))


def strip_digits(text):
    """
    Remove digits in the text
    """
    return ' '.join(''.join([_ for _ in text if not _.isdigit()]).split())


def merge_words(words):
    """
    Merge extracted words into one string, following specific rules
    Rules:
      - ['Macy', '\'s'] -> 'Macy\'s'
      - ['Best', 'Buy'] -> 'Best Buy'
    """
    result = ''
    for word in words:
        if result == '':
            result = word
        elif word == '\'s':
            result += word
        else:
            result += ' ' + word

    return result


def stem(text):
    """
    stem all words in text
    """
    toks = text.split()
    STEM_LOCK.acquire()
    stem_toks = [_STEMMER.stem(tok) for tok in toks]
    STEM_LOCK.release()
    return ' '.join(stem_toks)


def isplural(word):
    """
    Uses NLTK's Wordnet
    returns True if word is plural, False else
    return False if word isn't in WordNet
    """
    if word.lower() in _OOV_PLURALS:
        return True
    lemma = lemmatize(word, 'n')
    return True if word is not lemma else False


def replaced_string_once(repl, expr, source):
    """
    in string (s), replace repl by expr once
    """
    # use lookahead to match either \b or \B\$
    return re.sub(r'(?=\b|\B\$)%s(?=\b|\B\$)'% re.escape(repl), expr, source, 1)


def all_repl_once(source, toks, token_str='azerty'):
    """
    replace all toks in source
    toks: list
    """
    for tok in toks:
        source = replaced_string_once(tok, token_str, source)

    return source


def pos_tagging(text):
    """
    part-of-speech tagging (e.g., computer => NOUN)
    input: text is a list of words
    output: list of tuples -- (word, POS tag)
    """
    TAGG_LOCK.acquire()
    result = _POS_TAGGER.tag(text)
    TAGG_LOCK.release()
    return result


def lemmatize(word, pos):
    """
    lemmatize words (e.g., did => do)
    """
    LEMM_LOCK.acquire()
    result = _WORDNET_LEMMA.lemmatize(word, pos=pos)
    LEMM_LOCK.release()
    return result


def pluralize(words):
    """
    pluralize all the words in a given sentence/phrase
    input: single word or list of words
    output: a string with the words in the input pluralized and
    joined by space
    """
    pluralized_words = []
    if not isinstance(words, list):
        words = [words]
    for word in words:
        if not isplural(word):
            pluralized_words.append(INFLECT_ENGINE.plural(word))
        else:
            pluralized_words.append(word)

    return ' '.join(pluralized_words)


def singularize(words):
    """
    singularize all the words in the given sentence/phrase
    input: single word or list of words
    output: a string with the words in the input singularized and joined
    by space
    """
    singularized_words = []
    # convert input to a list if it is only a single word
    if not isinstance(words, list):
        words = [words]
    tagged_words = pos_tagging(words)
    for word, tag in tagged_words:
        if tag.startswith('NN'):
            word = lemmatize(word, 'n')
        singularized_words.append(word)

    return ' '.join(singularized_words)


def simple_singularize(words):
    """
    singularize all the words by dropping the trailing 's' from each word.

    words - single string or list of strings
    output - a string
    """
    singularized_words = []
    # convert input to a list if it is only a single word
    if not isinstance(words, list):
        words = [words]
    for word in words:
        if word.endswith('s'):
            singularized_words.append(word[:-1])
        else:
            singularized_words.append(word)

    return ' '.join(singularized_words)


def ispast(text):
    """
    return True if sentence is past tense
    Conservative: if any present tense verb present, return false
    Relevant POS Tags:
    - VBD Verb, past tense
    - VBN Verb, past participle
    - VBG Verb, gerund or present participle
    - VBP Verb, non-3rd person singular present
    - VBZ Verb, 3rd person singular present
    - VB  Verb, base form
    """

    # NOTE(JAH): removed VBZ. It's for sentences like: 'he has fun.' but will
    # return False for 'Has money been spent?'. Since we rarely have queries
    # like the former, removed it for now. Let's see how this works
    words = word_tokenize(text)
    tagged_words = pos_tagging(words)
    past = []
    for _, tag in tagged_words:
        if tag in ['VBD', 'VBN']:
            past.append(True)
        elif tag in ['VBG', 'VBP', 'VB']:
            past.append(False)

    return all(past)


def remove_redundant(text_list):
    """
    return a new list of text without the redundant items (i.e., case
    insensitive and singularized comparison).
    For example:
    - ['Burger', 'burger', 'Burgers'] -> ['Burger']
    """
    singularized_list = []
    new_list = []
    for text in text_list:
        singularized_text = singularize(text.lower().split(' '))
        if singularized_text not in singularized_list:
            singularized_list.append(singularized_text)
            new_list.append(text)

    return new_list


def format_acnt(acnt):
    """
    standardize account type expression
    1. remove 'account' at the end of the str if present
    2. lower case
    3. strip whitespaces
    """
    formatted_account = re.sub('account$', '', acnt.lower())
    formatted_account.strip()
    return formatted_account


def if_contain(big_str, small_str):
    """
    return true if all the words in small_str are also in big_str and in the
    same order, return false otherwise
    """
    long_words = word_tokenize(big_str)
    short_words = word_tokenize(small_str)

    if len(long_words) < len(short_words):
        return False

    return any(short_words == long_words[i:i+len(short_words)] \
        for i in range(len(long_words) - len(short_words) + 1))


def remove_special_chars(text):
    """
    Return a new string that is constructed from text without any special
    characters. In other words, only keep alnum and whitespace.
    """

    assert isinstance(text, str)

    stripped_text = ''
    for char in text:
        if char.isalnum() or char in string.whitespace:
            stripped_text += char

    return stripped_text

def preload():
    """
    Trigger internal models to be loaded into memory.
    """
    sentence_split('Hello. How are you?')
    word_tokenize('how are you')
    sentiment_analysis('thanks!')
    expand_contraction('don\'t')
    is_domain('wut')
    americanize_words('colour')
    stem('i am not sure what stem means')
    isplural('colors')
    pos_tagging(['i', 'am', 'a', 'computer'])
    lemmatize('did', 'v')
