"""
Command line interface for anomaly detection.
"""

from math import ceil
from pprint import pprint
import argparse

from embed_utils import load_data_list
from RankAggregator import (
    BordaCount, AverageRank
)
from AnomalyDetector import (
    AnomalyDetectorUSE, AnomalyDetectorSIF, AnomalyDetectorAVE
)

def display_outliers(ranked_list, per=0.1, display_all=False):
    """
    print subset of ranked_list to screen
    """
    if len(ranked_list) < 20 or display_all is True:
        max_print = len(ranked_list)
    else:
        max_print = int(ceil(per * len(ranked_list)))
    pprint(ranked_list[0:max_print])

def display_args():
    """
    get cli inputs
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--a_scale_parameter', required=False,
                        default='0.001',
                        help='scale parameter for SIF mean. '
                             'Default value is 0.001.')
    parser.add_argument('-t', '--truncated_component_number', required=False,
                        default='1',
                        help='Number of components for truncated SVD. '
                             'Default value is 1.')
    parser.add_argument('-f', '--dataset', required=True,
                        help='Path to dataset.')
    parser.add_argument('-p', '--stop_words', required=False,
                        help='True/False: if True, then filter stop words.')
    parser.add_argument('-m', '--model_type', required=True,
                        help='Model type: one of SIF, USE, AVE.')
    parser.add_argument('-d', '--distance_measure', required=False,
                        default='euclidean',
                        help='One of euclidean/cosine. Default is euclidean.')
    parser.add_argument('-g', '--rank_aggregation', required=False,
                        help='Rank aggregation method. One of borda/average.')
    parser.add_argument('-l', '--display_all', required=False,
                        help='Display results for all sentences.')
    args = vars(parser.parse_args())
    return args

def main():
    """
    parse command line inputs
    """
    args = display_args()
    data = load_data_list(args['dataset'])
    a_scale_param = float(args['a_scale_parameter']) \
        if args['a_scale_parameter'] else 0.001
    truncated_svd_n_comp = int(args['truncated_component_number']) \
        if args['truncated_component_number'] else 1
    filter_stop = False if args['stop_words'] == 'False' else True
    model_type = args['model_type']
    distance_measure = args['distance_measure']
    rank_aggregation = args['rank_aggregation'] \
        if args['rank_aggregation'] else None
    display_all = True if args['display_all'] == 'True' else False

    print('------------------------------------------------------------------')

    ### Universal Sentence Encoder (USE)
    if model_type.lower() == 'use' or rank_aggregation is not None:
        print('using Universal Sentence Encoder (USE) model using %s distance'
              % distance_measure)
        anomaly_detector_use = AnomalyDetectorUSE(distance_measure)
        anomaly_detector_use.embed_data(data)
        ranked_list_use = anomaly_detector_use.sort_data()
        if not rank_aggregation:
            display_outliers(ranked_list_use, display_all=display_all)
        else:
            use_sentences_ranked = [sen for (sen, _) in ranked_list_use]

    ### Smooth Inverse Frequency (SIF)
    if model_type.lower() == 'sif' or rank_aggregation is not None:
        print('using Smooth Inverse Frequency (SIF) model using %s distance'
              % distance_measure)
        anomaly_detector_sif = AnomalyDetectorSIF(distance_measure)
        anomaly_detector_sif.embed_data(data,
                                        filter_stop,
                                        a_scale_param,
                                        truncated_svd_n_comp)
        ranked_list_sif = anomaly_detector_sif.sort_data()
        if not rank_aggregation:
            display_outliers(ranked_list_sif, display_all=display_all)
        else:
            sif_sentences_ranked = []
            for pack in ranked_list_sif:
                if isinstance(pack, tuple):
                    sif_sentences_ranked.append(pack[0])
                else:
                    sif_sentences_ranked.append(pack)

    ### Average un-weighted word embedding (AVE)
    if model_type.lower() == 'ave':
        print('using Average un-weighted word embedding (AVE) model using'
              '%s distance' % distance_measure)
        anomaly_detector_ave = AnomalyDetectorAVE(distance_measure)
        anomaly_detector_ave.embed_data(data, filter_stop)
        ranked_list_ave = anomaly_detector_ave.sort_data()
        display_outliers(ranked_list_ave, display_all=display_all)

    ### Rank Aggregation
    if rank_aggregation is not None:
        if rank_aggregation.lower() == 'borda':
            rank_aggregator = BordaCount()
        elif rank_aggregation.lower() == 'average':
            rank_aggregator = AverageRank()
        ballots_list = [use_sentences_ranked, sif_sentences_ranked]
        candidates, scores = rank_aggregator.do_aggregation(ballots_list)
        ranked_list = list(zip(candidates, scores))
        display_outliers(ranked_list, display_all=display_all)

if __name__ == '__main__':
    main()
