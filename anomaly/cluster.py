"""
Command line tool for clustering MTurk Data
"""
import argparse
import operator
from pprint import pprint
from aiutils.anomalydetection.embed_utils import (
    load_glove_model, load_data_list,
    sif_embed_sent, universal_embedding,
    cluster_data, GLOVE_MODEL_PATH
)


def cluster_predictor(data_list, data, number, filter_messup=0):
    """
    Runs k-means clustering on list of sentence embeddings
    data_list: list of sentence embeddings
    data: list of sentences
    number: number of clusters
    filter_messup: number of times filtering returns [] (see below function)
    returns dictionary with sentences as keys, and cluster identity as value
    """
    prediction_dict = {}
    kmeans = cluster_data(number, data_list)
    for i in range(len(data) - filter_messup):
        prediction_dict[data[i]] = kmeans.labels_[i]
    prediction_dict = sorted(prediction_dict.items(),
                             key=operator.itemgetter(1))
    return prediction_dict


def sif_cluster(data, filter_stop, model, a_scale_param, num_clusters):
    """
    Runs end to end clustering function on list of sentences using SIF
    using the SIF embedding
    data: list of sentences
    filter_stop: boolean of whether or not to filter stop words
    model: dictionary of word embeddings
    a_scale_param: scale parameter for SIF mean
    num_clusters: number of expected clusters
    returns dictionary with key as sentences and value as cluster identity
    """
    data_dict = {}
    data_list = []
    filter_messup = 0
    if filter_stop is False:
        for sent in data:
            # pylint: disable=maybe-no-member
            dim = sif_embed_sent(
                sent.lower(), model, a_scale_param, data, False).size
            if dim != 1:
                data_dict[sent] = \
                    sif_embed_sent(
                        sent.lower(), model, a_scale_param, data, False
                    )
                data_list.append(
                    sif_embed_sent(
                        sent.lower(), model, a_scale_param, data, False)
                )
            else:
                filter_messup = filter_messup + 1
    else:
        for sent in data:
            # pylint: disable=maybe-no-member
            dim = sif_embed_sent(
                sent.lower(), model, a_scale_param, data, True).size
            if dim != 1:
                data_dict[sent] = \
                    sif_embed_sent(
                        sent.lower(), model, a_scale_param, data, True
                    )
                data_list.append(
                    sif_embed_sent(
                        sent.lower(), model, a_scale_param, data, True)
                )
            else:
                filter_messup = filter_messup + 1
    clusters = cluster_predictor(data_list, data, num_clusters, filter_messup)
    return clusters


def univ_cluster(data, num_clusters):
    """
    Runs clustering using the universal sentence encoder as embedding
    data: list of sentence embeddings
    num_clusters: number of expected clusters
    returns dictionary with key as sentences, and value as cluster identity
    """
    data_dict = {}
    data_list = universal_embedding(data)
    for index, item in enumerate(data):
        data_dict[item] = data_list[index]
    clusters = cluster_predictor(data_list, data, num_clusters, 0)
    return clusters


def main():
    """
    Main function
    Runs clustering as command line tool
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--a_scale_parameter", required=False,
                        help="scale parameter for SIF Mean. "
                             "Default value is 0.001")
    parser.add_argument("-f", "--dataset", required=True,
                        help="path to dataset")
    parser.add_argument("-n", "--num_clusters", required=True,
                        help="Number of clusters for K-Means")
    parser.add_argument("-s", "--stopWords", required=False,
                        help="Remove stop words from utterances? "
                             "True or False? True by default")
    parser.add_argument("-e", "--embedding", required=True,
                        help="Use univ or sif embedding")
    args = vars(parser.parse_args())
    data = load_data_list(args["dataset"])
    num_clusters = int(args["num_clusters"])
    embed_method = args["embedding"]
    a_scale_param = float(args["a_scale_parameter"]) \
        if args["a_scale_parameter"] else 0.001
    f_stop = False if args["stopWords"] == 'False' else True
    if embed_method.lower() == "univ":
        pprint('Using Universal Sentence Encoder')
        pprint(univ_cluster(data, num_clusters))
    else:
        model = load_glove_model(GLOVE_MODEL_PATH)
        pprint('Using Smooth Inverse Frequency Embedding')
        pprint(sif_cluster(data, f_stop, model, a_scale_param, num_clusters))


if __name__ == '__main__':
    main()
