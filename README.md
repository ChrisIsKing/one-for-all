# Setup Instructions

### Install Project dependencies

1. Run `make env` command to create your python virtual environment.

2. Run `souce venv/bin/activate` to activate your new virtual environment.

3. Run `make install` to install project dependencies.

### Configure Google Assistant SDK

1. Follow instructions [here](https://developers.google.com/assistant/sdk/guides/library/python/embed/config-dev-project-and-account) to configure you Developer Project and Account Settings.

2. Follow instructions [here](https://developers.google.com/assistant/sdk/guides/library/python/embed/register-device) to register your device model and download client secret key.

3. Place client secret key in credentials folders.

4. Generate credentials to be able to run the sample code and tools. Reference the JSON file you downloaded in a previous step.

```
 google-oauthlib-tool --scope https://www.googleapis.com/auth/assistant-sdk-prototype \
      --scope https://www.googleapis.com/auth/gcm \
      --save --headless --client-secrets /path/to/client_secret_client-id.json
```

5. You should see a URL displayed in the terminal:

```
Please visit this URL to authorize this application: https://...
```

6. Copy the URL and paste it into a browser (this can be done on any machine). The page will ask you to sign in to your Google account. Sign into the Google account that created the developer project.

7. After you approve the permission request from the API, a code will appear in your browser, such as "4/XXXX". Copy and paste this code into the terminal:

```
Enter the authorization code:
```