import google_assistant.google_assistant_config as api
import logging
import os


class GoogleAssistantServer:
    def __init__(self):
        self.grpc_channel = api.connect()

    def infer(self, query):
        try:
            response = api.query(query, self.grpc_channel)
            if response is None:
                response = "Sorry, I didn't get that"
            if os.path.exists('out.wav'):
                os.remove('out.wav')
            return str(response)
        except Exception as e:
            logging.error(e)
            logging.error('Unable to query Google SDK')



