import os
import io
import json
import click
import logging
import google.auth.transport.grpc
import google.auth.transport.requests
import google.oauth2.credentials
import google_assistant.assistant_helpers as assistant_helpers
import google_assistant.audio_helpers as audio_helpers
import multiprocessing as mp

# Imports the Google Cloud client library
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types

from google.assistant.embedded.v1alpha2 import (
    embedded_assistant_pb2,
    embedded_assistant_pb2_grpc
)
DEVICE_MODEL_ID = 'adasa-6ac41-adada-v9gry1'
DEVICE_ID = '2219bc80-ab01-11e8-ad14-10c37b94cc4e'
ASSISTANT_API_ENDPOINT = 'embeddedassistant.googleapis.com'
DEFAULT_GRPC_DEADLINE = 60 * 3 + 5
PLAYING = embedded_assistant_pb2.ScreenOutConfig.PLAYING

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.path.dirname(os.path.abspath(__file__)) + "/../credentials/adasa.json"


class GoogleAssistant(object):
    """Integrates with Google Assistant SDK to send & receive text based queries
        Args:
            language_code: language for the conversation.
            device_model_id: identifier of the device model.
            device_id: identifier of the registered device instance.
            display: enable visual display of assistant response.
            channel: authorized gRPC channel for connection to the
              Google Assistant API.
            deadline_sec: gRPC deadline in seconds for Google Assistant API call.
    """
    def __init__(self, language_code, device_model_id, device_id,
                 display, channel, deadline_sec):
        self.language_code = language_code
        self.device_model_id = device_model_id
        self.device_id = device_id
        self.conversation_state = None
        # Force reset of first conversation.
        self.is_new_conversation = True
        self.display = display
        self.assistant = embedded_assistant_pb2_grpc.EmbeddedAssistantStub(
            channel
        )
        self.deadline = deadline_sec
        self.conversation_stream = io.BytesIO()

    def __enter__(self):
        return self

    def __exit__(self, etype, e, traceback):
        if e:
            return False

    def assist(self, text_query):
        """Send a text request to the Assistant and playback the response.
        """
        def iter_assist_requests():
            config = embedded_assistant_pb2.AssistConfig(
                audio_out_config=embedded_assistant_pb2.AudioOutConfig(
                    encoding='LINEAR16',
                    sample_rate_hertz=16000,
                    volume_percentage=0,
                ),
                dialog_state_in=embedded_assistant_pb2.DialogStateIn(
                    language_code=self.language_code,
                    conversation_state=self.conversation_state,
                    is_new_conversation=self.is_new_conversation,
                ),
                device_config=embedded_assistant_pb2.DeviceConfig(
                    device_id=self.device_id,
                    device_model_id=self.device_model_id,
                ),
                text_query=text_query,
            )
            # Continue current conversation with later requests.
            self.is_new_conversation = False
            if self.display:
                config.screen_out_config.screen_mode = PLAYING
            req = embedded_assistant_pb2.AssistRequest(config=config)
            assistant_helpers.log_assist_request_without_audio(req)
            yield req

        text_response = None
        html_response = None
        no_text = False
        for resp in self.assistant.Assist(iter_assist_requests(),
                                          self.deadline):
            assistant_helpers.log_assist_response_without_audio(resp)
            if resp.screen_out.data:
                html_response = resp.screen_out.data
            if resp.dialog_state_out.conversation_state:
                conversation_state = resp.dialog_state_out.conversation_state
                self.conversation_state = conversation_state
            if resp.dialog_state_out.supplemental_display_text:
                text_response = resp.dialog_state_out.supplemental_display_text
            if len(resp.audio_out.audio_data) > 0 and text_response is None:
                self.conversation_stream.write(resp.audio_out.audio_data)
                no_text = True

        if no_text:
            with open('out.wav', "wb") as f:
                f.write(self.conversation_stream.getvalue())
                f.close()
            text_response = speech_convert(client)

        return text_response, html_response


def speech_convert(client):
    try:
        # Loads the audio into memory
        with io.open('out.wav', 'rb') as audio_file:
            content = audio_file.read()
            audio = types.RecognitionAudio(content=content)
    except Exception as e:
        print(e)
        logging.error('Unable to load converted audio into memory')
        response = "Didn't get that!"
        return response

    config = types.RecognitionConfig(
        encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
        sample_rate_hertz=16000,
        language_code='en-US')

    try:
        # Detects speech in the audio file
        response = client.recognize(config, audio)
    except Exception as e:
        print(e)
        logging.error('Unable to query Speech to Text API')
        response = "Didn't get that!"
        return response

    for result in response.results:
        response = format(result.alternatives[0].transcript)
    return response


def connect():

    credentials = os.path.join(click.get_app_dir('google-oauthlib-tool'), 'credentials.json')
    try:
        with open(credentials, 'r') as f:
            credentials = google.oauth2.credentials.Credentials(token=None,
                                                                **json.load(f))
            http_request = google.auth.transport.requests.Request()
            credentials.refresh(http_request)
    except Exception as e:
        logging.error('Error loading credentials: %s', e)
        logging.error('Run google-oauthlib-tool to initialize ')

    # Create an authorized gRPC channel.
    logging.getLogger().setLevel(logging.INFO)
    grpc_channel = google.auth.transport.grpc.secure_authorized_channel(
            credentials, http_request, ASSISTANT_API_ENDPOINT)
    logging.info('Connecting to %s', ASSISTANT_API_ENDPOINT)
    return grpc_channel


def query(input_query, grpc_channel):
    try:
        with GoogleAssistant('en-US', DEVICE_MODEL_ID, DEVICE_ID, False, grpc_channel, DEFAULT_GRPC_DEADLINE) \
                as assistant:
            response_text, response_html = assistant.assist(text_query=input_query)
            return response_text
    except Exception as e:
        print(e)
        logging.error('Connection successful, query request failed')
        return


def create_conversational_stream():
    output_audio_file = 'out.wav'
    audio_device = None
    audio_source = audio_device = (
            audio_device or audio_helpers.SoundDeviceStream(
                sample_rate=audio_helpers.DEFAULT_AUDIO_SAMPLE_RATE,
                sample_width=audio_helpers.DEFAULT_AUDIO_SAMPLE_WIDTH,
                block_size=audio_helpers.DEFAULT_AUDIO_DEVICE_BLOCK_SIZE,
                flush_size=audio_helpers.DEFAULT_AUDIO_DEVICE_FLUSH_SIZE
            )
    )

    audio_sink = audio_helpers.WaveSink(
        open(output_audio_file, 'wb'),
        sample_rate=audio_helpers.DEFAULT_AUDIO_SAMPLE_RATE,
        sample_width=audio_helpers.DEFAULT_AUDIO_SAMPLE_WIDTH
    )

    # Create conversation stream with the given audio source and sink.
    conversation_stream = audio_helpers.ConversationStream(
        source=audio_source,
        sink=audio_sink,
        iter_size=audio_helpers.DEFAULT_AUDIO_ITER_SIZE,
        sample_width=audio_helpers.DEFAULT_AUDIO_SAMPLE_WIDTH,
    )
    return conversation_stream


client = speech.SpeechClient()
