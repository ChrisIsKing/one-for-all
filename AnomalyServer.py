from anomaly.embed_utils import (
    euclidean_distance, sif_embed_sent, load_glove_model,
    GLOVE_MODEL_PATH, filter_sentence
)
from bert_serving.client import BertClient
import logging
import os


class AnomalyServer():
    """docstring forAnomalyServer."""
    def __init__(self):
        try:
            path = os.path.join("anomaly", GLOVE_MODEL_PATH)
            self.GLOVE = load_glove_model(path)
            self.bert_client = BertClient(check_length=False)
        except Exception as e:
            logging.error("Unable to load glove embeddings at path: " + path)
            print(e)

    def sim_search(self, utterance, responses):
        distance_dict = {}
        search_embed = sif_embed_sent(utterance, self.GLOVE, 0.001, utterance, True)
        count = 0
        for response in responses:
            sent_embed = sif_embed_sent(response, self.GLOVE, 0.001, utterance, True)
            distance_dict[count] = euclidean_distance(search_embed, sent_embed)
            count += 1
        return distance_dict

    def sim_search_bert(self, utterance, responses):
        distance_dict = {}
        search_embed = self.bert_client.encode([''.join(filter_sentence(utterance))])
        count = 0
        for response in responses:
            sent_embed = self.bert_client.encode([response])
            distance_dict[count] = euclidean_distance(search_embed, sent_embed)
            count += 1
        return distance_dict
