import adasa.adasa_config as api


class AdasaServer:
    def __init__(self):
        self.access_token, self.refresh_token = api.oauth("admin", "passwordsRedefined")

    def infer(self, query):
        try:
            response = api.infer(self.access_token, query)
            return response["visuals"]["formattedResponse"]
        except Exception as e:
            print(e)
            return "Didn't get that"
