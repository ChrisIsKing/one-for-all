import requests
import geocoder
import json

url = "https://automotive.clinc.ai/"


def oauth(username, password):
    payload = "username=" + username + "&password=" + password + "&grant_type=password"
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    response = requests.request('POST', url + 'v1/oauth', headers=headers, data=payload, allow_redirects=False, verify=False)
    response = json.loads(response.text)
    return response["access_token"], response["refresh_token"]


def infer(access_token, query):
    g = geocoder.ip('me')
    body = {
        "query": query,
        "lat": g.latlng[0],
        "lon": g.latlng[1],
        "time_offset": 300,
        "device": " ",
        "dialog": ""
    }
    headers = {
        'Content-Type': 'application/json',
        'Authorization': "Bearer " + access_token
    }
    payload = json.dumps(body)
    response = requests.request('POST', url + 'v1/query', headers=headers, data=payload, allow_redirects=False, verify=False)
    return json.loads(response.text)