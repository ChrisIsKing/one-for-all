import hound.houndify_config as api
import logging


class HoundifyServer:
    def __init__(self):
        self.client = api.connect()

    def infer(self, query):
        try:
            response = api.query(query, self.client)
        except Exception as e:
            logging.error('Unable to query Hound SDK')
            response = 'Unable to connect to Hound api'
        if response is None or response == '':
            response = 'Didn\'t get that'
        return response['AllResults'][0]['SpokenResponseLong']