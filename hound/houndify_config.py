import hound.houndify as houndify
import logging
import geocoder

CLIENT_ID = 'axML4r8STDG5lncVzhJUaw=='
CLIENT_KEY = 'gecFkOcFAGLUnlvcmTyvVeLFp57su21uk7UaN8T0Y5YsoaYNQ5Jiv6jl9ajAmQ8jn7Bkl3Ybx8fOUNMNyWoKkQ=='


def connect():
    try:
        logging.info('Connecting to Hound API')
        g = geocoder.ip('me')
        request_info = {
          # Todo: Change to vehicle location
          'Latitude': g.latlng[0],
          'Longitude': g.latlng[1]
        }
        client = houndify.TextHoundClient(CLIENT_ID, CLIENT_KEY, "test_user", request_info)
        return client
    except Exception as e:
        print(e)
        logging.error('Unable to connect to Hound API endpoint')
        return None


def query(text_input, client):
    if client is None:
        client = connect()
    try:
        response = client.query(text_input)
    except Exception as e:
        logging.error('Unable to query Hound API')
        return None

    return response
