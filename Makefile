env:
	virtualenv --python=python3.6 venv

install:
	pip install -r requirements.txt

start:
	bert-serving-start -model_dir ./anomaly/utils/bert/ -num_worker=1
	python app.py

linux:
	sudo apt-get update
	sudo apt-get install python3.6
	sudo apt-get install python3.6-dev
