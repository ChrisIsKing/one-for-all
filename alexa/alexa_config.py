import logging
import io
import os

# Imports the Google Cloud client library
from google.cloud import texttospeech
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types

from avs_client import AlexaVoiceServiceClient
from subprocess import run

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.path.dirname(os.path.abspath(__file__)) + "/../credentials/adasa.json"

def avs_connect():
    try:
        #logging.info('Connecting to Alexa API')
        alexa_client = AlexaVoiceServiceClient(
            client_id='amzn1.application-oa2-client.235bc5f311ac422f96f5f296518b0fbc',
            secret='300d8bc8e924816068d055185b36832806af1ddb52ca02f4b6c5dbdb0fc4a2d1',
            refresh_token='Atzr|IwEBIGYi5m7FPapja6MRTwioKLsM245Ql1kvwoyUvbtk1XZoK7pZE5q-WDkDFgjQT_6n7qqOxkkwO_J8oPsOog'
                          '1AkMFO-GZz8wgoutN26eDQ4hvpLBD_awz81L0pOpBSQg6uZ6rgiZHNU5hb213j_CS6m0TN3xF3jBn2g_EfZZI7gI2aeh'
                          'ZLUf697TmELiAniiiSQJPi1LX2vMMK4BbRN8WHsDP702DJquLn6jgcV1-aVBnBxFL6UO2rmsdR-YYtJu4RLGjNIImo8eL'
                          'zddwZZA2ND8ET5oiuRGIpix-_nDOjmAZcTSJIXDHm5fU8MLSoHn1iisX3ZQ6gWtVYgvYM1C_Cwyy0YdClMG8elzlEab3P'
                          'yvEvR2HQ5sTNfTk6iTiy45ixGjCJjnLIt27cGsz5wIyip24lXq_WSlX-xwesbqKbqpMlG_tlESyB7EiFs89bhcKB0UI',
        )
        alexa_client.connect()
        return alexa_client
    except Exception as e:
        print(e)
        return None

def text_to_speech_connect():
    # Instantiates a client
    client = texttospeech.TextToSpeechClient()
    return client


def speech_to_text_connect():
    # Instantiates a client
    client = speech.SpeechClient()
    return client


def text_convert(text_query, client):
    # Check if client is instantiated
    if client is None:
        client = text_to_speech_connect()

    # Set the text input to be synthesized
    synthesis_input = texttospeech.types.SynthesisInput(text=text_query)

    # Build the voice request, select the language code ("en-US") and the ssml
    # voice gender ("neutral")
    voice = texttospeech.types.VoiceSelectionParams(
        language_code='en-US',
        ssml_gender=texttospeech.enums.SsmlVoiceGender.NEUTRAL)

    # Select the type of audio file you want returned
    audio_config = texttospeech.types.AudioConfig(
        audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16)

    # Perform the text-to-speech request on the text input with the selected
    # voice parameters and audio file type
    response = client.synthesize_speech(synthesis_input, voice, audio_config)
    return response


def speech_convert(client, audio_content):
    try:
        # write audio to file
        with open('alexa_audio.mp3', 'wb') as audio_file:
            audio_file.write(audio_content)
            audio_file.close()
    except Exception as e:
        logging.error('Unable to write audio to file')
        response = "Didn't get that!"
        return response

    run(["sox","alexa_audio.mp3", "-b", "16", "alexa_converted.wav", "channels", "1", "rate", "16k"])

    try:
        # Loads the audio into memory
        with io.open('alexa_converted.wav', 'rb') as audio_file:
            content = audio_file.read()
            audio = types.RecognitionAudio(content=content)
    except Exception as e:
        logging.error('Unable to load converted audio into memory')
        return

    config = types.RecognitionConfig(
        encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
        sample_rate_hertz=16000,
        language_code='en-US')

    try:
        # Detects speech in the audio file
        response = client.recognize(config, audio)
    except Exception as e:
        print(e)
        logging.error('Unable to query Speech to Text API')
        response = "Didn't get that!"
    return response


def query(audio, alexa_client):
    try:
        response = alexa_client.send_audio_file(audio)
        return response, alexa_client
    except Exception as e:
        alexa_client = avs_connect()
        response = alexa_client.send_audio_file(audio)
        return response, alexa_client


