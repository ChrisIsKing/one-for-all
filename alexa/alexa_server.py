import alexa.alexa_config as api
import logging
import os


class AlexaServer:
    def __init__(self):
        self.alexa_client = api.avs_connect()
        self.speech_to_text_client = api.speech_to_text_connect()

    def infer(self, audio_bytes):
        try:
            response, self.alexa_client = api.query(audio_bytes, self.alexa_client)
            if response is None:
                return "Sorry I don't understand"
            text = api.speech_convert(self.speech_to_text_client, response)
            if hasattr(text, 'results') and len(text.results) != 0:
                for result in text.results:
                    response = format(result.alternatives[0].transcript)
                    os.remove("alexa_converted.wav")
            else:
                response = "Sorry I don't understand"
            os.remove("alexa_audio.mp3")
        except Exception as e:
            print(e)
            logging.error("Alexa query failed")
            response = "Alexa query failed"
            return response
        try:
            response = response.decode('utf-8', 'ignore')
            return response
        except AttributeError:
            return str(response)


